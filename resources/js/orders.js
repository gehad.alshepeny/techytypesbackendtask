window.Vue = require('vue');



var app = new Vue({
    el: '#vue-wrapperOrders',
    data: {
        orderitems: [],
        isShowingError: false,
        isShowingSuccess: false,
        showModal: false,
    },

    mounted: function mounted() {
        this.getOrders();
    },
    methods: {

        getOrders: function getOrders() {
            var _this = this;
            axios.get('/TechTypesTask/public/getOrders').then(function (response) {
                _this.orderitems = response.data;
            });

        },

    }
});