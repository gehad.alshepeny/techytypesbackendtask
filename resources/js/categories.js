window.Vue = require('vue');


Vue.component('modal', {
    template: '#modal-template'
})

var app = new Vue({
    el: '#vue-wrapper',
    data: {
        catItems: [],
        isShowingError: false,
        isShowingSuccess: false,
        showModal: false,
        e_name: '',
        e_id: '',
        newItem: { 'name': '' },
    },
    mounted: function mounted() {
        this.getCategories();
    },
    methods: {
        getCategories: function getCategories() {
            var _this = this;
            axios.get('/TechTypesTask/public/getCategories').then(function (response) {
                _this.catItems = response.data;
            });
        },
        setVal(val_id, val_name) {
            this.e_id = val_id;
            this.e_name = val_name;
        },
        createCategory: function createCategory() {
            var _this = this;
            var input = this.newItem;
            if (input['name'] == '') {
                this.isShowingError = true;
            } else {
                this.isShowingError = false;
                axios.post('/TechTypesTask/public/storeCategory', input).then(function (response) {
                    _this.newItem = { 'name': '' };
                    _this.getCategories();
                });
                this.hasDeleted = true;
            }
        },
        editCategory: function () {
            var cat_id = document.getElementById('e_id');
            var cat_title = document.getElementById('e_name');
            axios.post('/TechTypesTask/public/editCategory/' + cat_id.value, { category_title: cat_title.value })
                .then(response => {
                    this.getCategories();
                    this.showModal = false
                });
            this.hasDeleted = true;

        },
        deleteCategory: function deleteCategory(item) {
            var _this = this;
            if (confirm("Do you really want to delete?")) {
                axios.post('/TechTypesTask/public/getCategories/' + item.id).then(function (response) {
                    _this.getCategories();
                    _this.isShowingSuccess = true,
                        _this.isShowingError = false
                });
            }
        }
    }
});