window.Vue = require('vue');


Vue.component('modal', {
    template: '#modal-template'
})

var app = new Vue({
    el: '#vue-wrapperProducts',
    data: {
        items: [],
        catItems: [],
        isShowingError: false,
        isShowingSuccess: false,
        showModal: false,
        e_name: '',
        e_id: '',
        e_price: '',
        e_category_id: '',
        newItem: { 'name': '', 'price': '', 'category_id': '' },
    },

    mounted: function mounted() {
        this.getProducts();
        this.getCategories();
    },
    methods: {
        getCategories: function getCategories() {
            var _this = this;

            axios.get('/TechTypesTask/public/getCategories').then(function (response) {

                _this.catItems = response.data;
            });
        },
        getProducts: function getProducts() {
            var _this = this;
            axios.get('/TechTypesTask/public/getProducts').then(function (response) {
                _this.items = response.data;
            });

        },
        setVal(val_id, val_name, val_price, val_category_id) {
            this.e_id = val_id;
            this.e_name = val_name;
            this.e_price = val_price;
            this.e_category_id = val_category_id;

        },

        createProduct: function createProduct() {
            var _this = this;
            var input = this.newItem;
            if (input['name'] == '' || input['price'] == '' || input['category_id'] == '') {
                this.isShowingError = true;
            } else {

                this.isShowingError = false;
                axios.post('/TechTypesTask/public/add_Product', input).then(function (response) {

                    _this.newItem = { 'name': '', 'price': '', 'category_id': '' };
                    _this.getProducts();
                    _this.getCategories();
                });
                this.isShowingSuccess = false;
            }
        },
        editProduct: function () {
            var id = document.getElementById('e_id');
            var title = document.getElementById('e_name');
            var price = document.getElementById('e_price');
            var cat = document.getElementById('e_category_id');
            axios.post('/TechTypesTask/public/editProduct/' + id.value, { product_title: title.value, product_price: price.value, product_cat: cat.value })
                .then(response => {
                    this.getProducts();
                    this.getCategories();
                    this.showModal = false
                });
            this.isShowingSuccess = false;

        },
        deleteProduct: function deleteProduct(item) {
            var _this = this;
            if (confirm("Do you really want to delete?")) {
                axios.post('/TechTypesTask/public/getProducts/' + item.id).then(function (response) {
                    _this.getProducts();
                    _this.getCategories();
                    _this.isShowingSuccess = true,
                        _this.isShowingError = false
                });
            }
        }
    }
});