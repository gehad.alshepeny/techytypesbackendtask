
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8" />
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
   @yield('title')
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="{{ asset('assets/maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css') }}">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-dashboard.minf066.css?v=2.1.0') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('assets/demo/demo.css') }} " rel="stylesheet" />
 <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<style>
  .btn{ text-transform: none !important; }
  .card-header-rose{background: linear-gradient(60deg,#3f044e,#3f044e)!important;}
  .card [class*=card-header-] .card-icon, .card [class*=card-header-] .card-text  {

    background-color: #3f044e !important;
  }
</style>
<body class="">

  <div class="wrapper" style="background-image: url({{ asset('assets/img/bg.jpg') }}); background-size: cover; background-position: top center;" >
    <!--style="background-image: url('../public/assets/img/bg.jpg'); background-size: cover; background-position: top center;" -->
    <div class="sidebar" data-color="purple" data-background-color="white" style="background-color:white">

      <div class="logo">
        <a href="{{ url('/dashboard') }}" class="simple-text logo-mini">
          
        </a>
        <a href="{{ url('/dashboard') }}" class="simple-text logo-normal">
        Task Dashboard
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="{{ asset('assets/img/default-avatar.png') }}" />
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                Welcome Admin
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
            

                <li class="nav-item">

                  <a class="nav-link" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                    <i class="material-icons">lock</i>  {{ __('Logout') }}

                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>


              </ul>
            </div>
          </div>
        </div>
        <?php $str= request()->path();  ?>
        <ul class="nav">
       
          <li class="nav-item {{strpos($str, 'products') !== false || strpos($str, 'addProject_images') || strpos($str, 'edit_image')  !== false ? 'active': '' }}">
            <a class="nav-link" href="{{ url('/products') }}">
              <i class="material-icons">assignment</i>
              <p> Products</p>
            </a>
          </li>
          <li class="nav-item {{strpos($str, 'categories') !== false ? 'active': '' }}">
            <a class="nav-link" href="{{ url('/categories') }}">
              <i class="material-icons">toc</i>
              <p> Categories</p>
            </a>
          </li>

          <li class="nav-item {{strpos($str, 'orders') !== false ? 'active': '' }} ">
            <a class="nav-link" href="{{ url('/orders') }}">
              <i class="material-icons">add_shopping_cart</i>
              <p> Orders </p>
            </a>
          </li>

         

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">@yield('toptitle')</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>

        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="content">
              @yield('content')



                    </div>
                  </div>

                </div>
              </div>

              <!--   Core JS Files   -->
              
              <script type="text/javascript" src= "{{ url('js/app.js') }}"></script>
              <script src="{{ asset('assets/js/core/jquery.min.js') }}"></script>
              <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
              <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>
              <script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
              <!-- Plugin for the momentJs  -->
              <script src="{{ asset('assets/js/plugins/moment.min.js')}}"></script>
              <!--  Plugin for Sweet Alert -->
              <script src="{{ asset('assets/js/plugins/sweetalert2.js')}}"></script>
              <!-- Forms Validations Plugin -->
              <script src="{{ asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
              <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
              <script src="{{ asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
              <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
              <script src="{{ asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
              <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
              <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
              <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
              <script src="{{ asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
              <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
              <script src="{{ asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
              <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
              <script src="{{ asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
              <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
              <script src="{{ asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
              <script src="{{ asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
              <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
              <script src="{{ asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
              <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
              <script src="{{ asset('assets/js/plugins/nouislider.min.js')}}"></script>
              <script src="{{ asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
              <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
              <script src=" {{ asset('assets/cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js')}}"></script>
              <!-- Library for adding dinamically elements -->
              <script src="{{ asset('assets/js/plugins/arrive.min.js')}}"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
              <!-- Place this tag in your head or just before your close body tag. -->
              <script async defer src=" {{ asset('assets/buttons.github.io/buttons.js')}}"></script>
              <!-- Chartist JS -->
              <script src="{{ asset('assets/js/plugins/chartist.min.js')}}"></script>
              <!--  Notifications Plugin    -->
              <script src="{{ asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="{{ asset('assets/js/material-dashboard.minf066.js?v=2.1.0')}}" type="text/javascript"></script>
              <!-- Material Dashboard DEMO methods, don't include it in your project! -->
              <script src="{{ asset('assets/demo/demo.js')}}"></script>
              <!-- Sharrre libray -->
              <script src="{{ asset('assets/demo/jquery.sharrre.js')}}"></script>
              <script>
                $(document).ready(function() {
                  $('#datatables').DataTable({
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });

                  var table = $('#datatable').DataTable();

                  // Edit record
                  table.on('click', '.edit', function() {
                    $tr = $(this).closest('tr');
                    var data = table.row($tr).data();
                    alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
                  });

                  // Delete a record
                  table.on('click', '.remove', function(e) {
                    $tr = $(this).closest('tr');
                    table.row($tr).remove().draw();
                    e.preventDefault();
                  });

                  //Like record
                  table.on('click', '.like', function() {
                    alert('You clicked on Like button');
                  });
                });
              </script>
              <script>
    $(document).ready(function() {
      // initialise Datetimepicker and Sliders
      md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }
    });
  </script>


<script>
$("document").ready(function(){
    setTimeout(function(){
       $("div.alert").remove();
    }, 2000 ); // 5 secs

});
</script>
  @yield('scripts')
</body>
</html>
