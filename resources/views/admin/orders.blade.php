@extends('layouts.admin')
@section('title')
 Dashboard |Orders
@endsection
@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">

        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">add_shopping_cart</i>
          </div>
          <h4 class="card-title">Orders</h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
    <div class="flex-center position-ref full-height">
        <div id="vue-wrapperOrders">
            <div class="content">
                <div class="table table-borderless" id="table">
                    <table class="table table-borderless" id="table">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>User name</th>
                                <th>Date</th>
                                <th>Order Details</th>
                            </tr>
                        </thead>
                        <tr v-for="orderitem in orderitems.data">
                            <td>@{{ orderitem.id }}</td>
                            <td>@{{ orderitem.user_name }}</td>
                            <td>@{{ orderitem.order_date }} </td>
                            <td id="show-modal" @click="showModal=true; "  class="btn btn-info" ><i class="material-icons">remove_red_eye</i></td>
                            
                        </tr>
                    </table>
                    
                </div>
               

            </div>
        </div>
    </div>
    
 

          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection
