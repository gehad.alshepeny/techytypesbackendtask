@extends('layouts.admin')
@section('title')
 Dashboard |Categories
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">

        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">toc</i>
          </div>
          <h4 class="card-title">Categories</h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
    <div class="flex-center position-ref full-height">
        <div id="vue-wrapper">
            <div class="content">
                        
                  <div class="form-group">
                    <label for="name">Category Title:</label>
                    <input type="text" class="form-control" id="name" name="name" 
                        required v-model="newItem.name" >
                  </div>

                 <button class="btn btn-primary" @click.prevent="createCategory()" id="name" name="name">
                    <span class="glyphicon glyphicon-plus"></span> ADD
                 </button>

                <p class="text-center alert alert-danger"
                v-show="isShowingError" >Please fill all fields!</p>
                {{ csrf_field() }}
                <p  class="text-center alert alert-success"
                v-show="isShowingSuccess">Deleted Successfully!</p>
                <div class="table table-borderless" id="table">
                    <table class="table table-borderless" id="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Category Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tr v-for="item in catItems">
                            <td>@{{ item.id }}</td>
                            <td>@{{ item.title }}</td>
                            <td id="show-modal" @click="showModal=true; setVal(item.id, item.title)"  class="btn btn-info" ><i class="material-icons">edit</i></td>
                            <td @click.prevent="deleteCategory(item)" class="btn btn-danger btn-just-icon remove"><i class="material-icons">delete</i></td>
                        </tr>
                    </table>
                </div>
                <modal v-if="showModal" @close="showModal=false">
                    <h3 slot="header">Edit Category</h3>
                    <div slot="body">
                        <input type="hidden" disabled class="form-control" id="e_id" name="id"
                                required  :value="this.e_id">
                        Category Title: <input type="text" class="form-control" id="e_name" name="name"
                                required  :value="this.e_name">
                    </div>
                    <div slot="footer">
                        <button class="btn btn-default" @click="showModal = false">
                        Cancel
                      </button>
                      
                      <button class="btn btn-info" @click="editCategory()">
                        Update
                      </button>
                    </div>
                </modal>
            </div>
        </div>
    </div>
    
    <script type="text/x-template" id="modal-template">
      <transition name="modal">
        <div class="modal-mask">
          <div class="modal-wrapper">
            <div class="modal-container">

              <div class="modal-header">
                <slot name="header">
                  default header
                </slot>
              </div>

              <div class="modal-body">
                <slot name="body">
                    
                </slot>
              </div>

              <div class="modal-footer">
                <slot name="footer">
                  
                  
                </slot>
              </div>
            </div>
          </div>
        </div>
      </transition>
    </script>

          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection
