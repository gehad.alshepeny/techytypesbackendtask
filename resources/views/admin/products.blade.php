@extends('layouts.admin')
@section('title')
 Dashboard |Products
@endsection
@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">

        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">assignment</i>
          </div>
          <h4 class="card-title">Products</h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
    <div class="flex-center position-ref full-height">
        <div id="vue-wrapperProducts">
            <div class="content">
                        
                  <div class="form-group">
                    <label for="name">Product Title:</label>
                    <input type="text" class="form-control" id="name" name="name" 
                        required v-model="newItem.name" >
                  </div>
                         
                  <div class="form-group">
                    <label for="name">Price:</label>
                    <input type="text" class="form-control" id="price" name="price" 
                        required v-model="newItem.price" >
                  </div>
                         
                  <div class="form-group">
                    <label for="name">Category:</label>
                        <select class="form-control" data-size="7" id="category_id" name="category_id"  
                        required v-model="newItem.category_id" >
                            <option value="">Select</option>
                            <option v-for='data in catItems' :value='data.id'>@{{ data.title }}</option>
                          </select>
                  </div>

                 <button class="btn btn-primary" @click.prevent="createProduct()" id="name" name="name">
                    <span class="glyphicon glyphicon-plus"></span> ADD
                 </button>
           

                <p class="text-center alert alert-danger"
                v-show="isShowingError" >Please fill all fields!</p>
                {{ csrf_field() }}
                <p  class="text-center alert alert-success"
                v-show="isShowingSuccess">Deleted Successfully!</p>
                <div class="table table-borderless" id="table">
                    <table class="table table-borderless" id="table">
                        <thead>
                            <tr>
                                <th>Product Title</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tr v-for="item in items.data">
                          
                            <td>@{{ item.title }}</td>
                            <td>@{{ item.price }}</td>
                            <td>@{{ item.category_name }}</td>
                            <td id="show-modal" @click="showModal=true; setVal(item.id, item.title,item.price, item.category_id)"  class="btn btn-info" ><i class="material-icons">edit</i></td>
                            <td @click.prevent="deleteProduct(item)" class="btn btn-danger btn-just-icon remove"><i class="material-icons">delete</i></td>
                        </tr>
                    </table>
                    
                </div>
                <modal v-if="showModal" @close="showModal=false">
                    <h3 slot="header">Edit Product</h3>
                    <div slot="body">
                        <input type="hidden" disabled class="form-control" id="e_id" name="id"
                                required  :value="this.e_id">
                        Name: <input type="text" class="form-control" id="e_name" name="name"
                                required  :value="this.e_name">
                        Price: <input type="text" class="form-control" id="e_price" name="price"
                        required  :value="this.e_price">
                        <div class="form-group">
                    <label for="name">Category:</label>
                        <select class="form-control" data-size="7" id="e_category_id" name="category_id"  
                        required v-model="this.e_category_id" >
                            <option value="">Select</option>
                            <option v-for='data in catItems' :value='data.id'>@{{ data.title }}</option>
                          </select>
                  </div>
                        
                      
                    </div>
                    <div slot="footer">
                        <button class="btn btn-default" @click="showModal = false">
                        Cancel
                      </button>
                      
                      <button class="btn btn-info" @click="editProduct()">
                        Update
                      </button>
                    </div>
                </modal>

            </div>
        </div>
    </div>
    
    <script type="text/x-template" id="modal-template">
      <transition name="modal">
        <div class="modal-mask">
          <div class="modal-wrapper">
            <div class="modal-container">

              <div class="modal-header">
                <slot name="header">
                  default header
                </slot>
              </div>

              <div class="modal-body">
                <slot name="body">
                    
                </slot>
              </div>

              <div class="modal-footer">
                <slot name="footer">
                  
                  
                </slot>
              </div>
            </div>
          </div>
        </div>
      </transition>
    </script>

          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection
