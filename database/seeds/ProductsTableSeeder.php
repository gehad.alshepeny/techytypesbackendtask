<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'title' => 'Product name testing',
            'categories_id' => 1,
            'price' => 500,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('products')->insert([
            'title' => 'New Product name ',
            'categories_id' => 1,
            'price' => 600,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('products')->insert([
            'title' => 'Mobile',
            'categories_id' => 3,
            'price' => 2600,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
