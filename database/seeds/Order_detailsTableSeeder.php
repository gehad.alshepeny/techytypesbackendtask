<?php

use Illuminate\Database\Seeder;

class Order_detailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_details')->insert([
            'orders_id' => 1,          
            'products_id' => 1,          
            'quantity' => 3, 
            'created_at' => now(),
            'updated_at' => now(),               
        ]);
        DB::table('order_details')->insert([
            'orders_id' => 1,          
            'products_id' => 3,          
            'quantity' => 4, 
            'created_at' => now(),
            'updated_at' => now(),               
        ]);
        DB::table('order_details')->insert([
            'orders_id' => 2,          
            'products_id' => 1,          
            'quantity' => 1,   
            'created_at' => now(),
            'updated_at' => now(),             
        ]);
    }
}
