<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_details extends Model
{
    protected $fillable = [ 'orders_id','products_id','quantity', ];

    public function order()
    {
   return $this->belongsTo('App\Orders', 'orders_id');
    }
}
