<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Products;
use App\Http\Resources\ProductsResource;


class ProductsController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    public function allProducts(Request $request)
    {
        $products=Products::where('id', '!=', 0);
        if($request->title){
            $products->where('title','LIKE','%'.$request->title."%");
        }
        if($request->price_from)
        {
            $products->where('price','>=',$request->price_from);
        }
        if($request->price_to)
        {
            $products->where('price','<=',$request->price_to);
        }
        if($request->category)
        {
            $products->where('categories_id','=',$request->category);
        }
        if($request->sortCol && $request->sortDir)
        {
            $products->orderBy($request->sortCol,$request->sortDir);
        }

        $allProducts=$products->get();

        return ProductsResource::collection($allProducts);
    }
  
}
