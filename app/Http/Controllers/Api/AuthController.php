<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required|min:6'
        ]);
        if (User::where('email', '=', $request->email)->exists()) {
            return response()->json(['message' => 'This email already exist!'], 404);
        }else{
            $user = User::create([
                'name' => $request->name, 
                'usertype' => 'customer', 
                'email' => $request->email, 
                'password' => bcrypt($request->password),
                'email_verified_at' => now(),
            ]);

        }

        return response()->json($user);
    }

   public function login(Request $request)
   {
    $request->validate([
        'email' => 'required|email|exists:users,email', 
        'password' => 'required'
    ]);

    if( Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
        $user = Auth::User();

        $token = $user->createToken($user->email.'-'.now());
       
        return response()->json([
            'token' => $token->accessToken
        ]);

    }
    else{
        return response()->json(['message' => 'Invalid Credentials!'], 404);

    }
}
}
