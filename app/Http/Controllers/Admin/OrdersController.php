<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orders;
use App\Order_details;
use App\Http\Resources\OrdersResource;
use Auth;

class OrdersController extends Controller
{
    public function index()
    {
      return view('admin.orders');
    }
    public function getOrders() {
        $orders = Orders::orderBy('created_at', 'DESC')->get();
          return OrdersResource::collection($orders);
      }
    public function placeOrder(Request $request)
    {
        @$OrderDetails = $request['order_details'] ;
        if( @$OrderDetails ) {
            $user_id=Auth::user()->id;
            $data = new Orders ();
            $data->user_id = $user_id;
            $data->save ();
            $OrderId = $data ->id ;
    foreach ( $OrderDetails as $order ) {
      Order_details::create([
        'orders_id' => $OrderId     ,
        'products_id' => $order['product_id'] ,
        'quantity' => $order['quantity'] ,
      ]);
    };
    return response()->json(['message' => 'Order Sent!']);
}    else{
    return response()->json(['message' => 'Order not found!'], 404);

}
        
    }
}
