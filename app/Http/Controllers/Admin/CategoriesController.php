<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;

class CategoriesController extends Controller
{
    public function index()
    {
      return view('admin.categories');
    }
    public function storeCategory(Request $request) {
      $data = new Categories ();
      $data->title = $request->name;
      $data->save ();
      return $data;
    }
    public function getCategories() {
      $data = Categories::all ();
      return $data;
    }
    public function editCategory(Request $request, $id){
      $data =Categories::where('id', $id)->first();
      $data->title = $request->get('category_title');
      $data->save();
      return $data;
    }
    public function deleteCategory(Request $request) {
      $data = Categories::find ( $request->id )->delete ();
    }
   
}
