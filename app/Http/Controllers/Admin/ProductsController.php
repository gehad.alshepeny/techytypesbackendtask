<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Categories;
use App\Http\Resources\ProductsResource;

class ProductsController extends Controller
{
    public function index()
    {
      return view('admin.Products');
    }
    public function saveProductData(Request $request) {
      $data = new Products ();
      $data->title = $request->name;
      $data->price = $request->price;
      $data->categories_id = $request->category_id;
      $data->save ();
      return $data;
    }
    public function getProducts() {
      $products = Products::orderBy('price', 'DESC')->get();
        return ProductsResource::collection($products);
    }
    public function editProduct(Request $request, $id){
      $data =Products::where('id', $id)->first();
      $data->title = $request->get('product_title');
      $data->price = $request->get('product_price');
      $data->categories_id = $request->get('product_cat');
      $data->save();
      return $data;
    }
    public function deleteProduct(Request $request) {
      $data = Products::find ( $request->id )->delete ();
    }
   
}
