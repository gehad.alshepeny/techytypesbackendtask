<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'title','created_at','updated_at',
      ];


      public function products()
      {
        return $this->hasMany('App\Products');
      }
}
