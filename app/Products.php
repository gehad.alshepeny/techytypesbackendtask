<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'title','categories_id','price','created_at','updated_at',
      ];

    public function categories()
    {
        return $this->belongsTo('App\Categories', 'categories_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Orders');
    }

   
}
