<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [ 'user_id','created_at','updated_at', ];

    public function user()
    {
   return $this->belongsTo('App\User', 'user_id');
    }
    public function order_details()
    {
        return $this->hasMany('App\Order_details');
    }
    public function product()
    {
   return $this->belongsTo('App\Products', 'products_id');
    }
    
}
