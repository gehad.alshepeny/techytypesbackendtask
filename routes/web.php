<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth','admin']],function(){

  Route::get('/', function () {
      return view('admin.dashboard');
  });
  Route::get('/dashboard', function () {
      return view('admin.dashboard');
  });
 
  Route::get('/products', 'Admin\productsController@index');
  Route::get ( '/getProducts', 'Admin\productsController@getProducts' );
  Route::post('add_Product','Admin\productsController@saveProductData');
  Route::post ( '/getProducts/{id}', 'Admin\productsController@deleteProduct' );
  Route::post ( '/editProduct/{id}', 'Admin\productsController@editProduct' );




  Route::get('/categories', 'Admin\CategoriesController@index');
  Route::post ( 'storeCategory', 'Admin\CategoriesController@storeCategory' );
  Route::get ( '/getCategories', 'Admin\CategoriesController@getCategories' );
  Route::post ( '/getCategories/{id}', 'Admin\CategoriesController@deleteCategory' );
  Route::post ( '/editCategory/{id}', 'Admin\CategoriesController@editCategory' );

  Route::get('/orders', 'Admin\OrdersController@index');
  Route::get ( '/getOrders', 'Admin\OrdersController@getOrders' );

  



});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');
